import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#1
df = pd.read_csv('home_work/etc/WHC.csv')

to_delete = []
for column in df:
    if 'Unnamed' in column or column == '':
        to_delete.append(column)
df = df.drop(columns=to_delete)

#2
df['Year'] = df['Year'].astype('object')

#3
df['happiness_score'] = df.groupby('Country')['happiness_score'].transform(lambda x: x.fillna(x.mean()))
#4
q75, q25 = np.percentile(df['happiness_score'], [75, 25])
iqr = q75 - q25
# print(iqr, q75, q25)
#  1.6950101349999995  6.3004999999999995 4.605489865
res_4 = pd.DataFrame()
res_4 = df.loc[(df['happiness_score'] >= q25) & (df['happiness_score'] <= q75)]
res_4 = res_4.drop_duplicates('Country', keep='first')
res_4 = res_4.sort_values('Country')
list_of_countries = list(res_4['Country'])
# print(list_of_countries)

#5

df_max = df.max(numeric_only=True)
df_min = df.min(numeric_only=True)
df_mean = df.mean(numeric_only=True)
df_std = df.std(numeric_only=True)

countries_max = df.groupby('Country')['happiness_score'].max()
countries_min = df.groupby('Country')['happiness_score'].min()
countries_mean = df.groupby('Country')['happiness_score'].mean()
countries_std = df.groupby('Country')['happiness_score'].std()

arg_max = df.loc[df['Country'] == 'Argentina', 'happiness_score'].max()
alg_max = df.loc[df['Country'] == 'Algeria', 'happiness_score'].max()

arg_min = df.loc[df['Country'] == 'Argentina', 'happiness_score'].min()
alg_min = df.loc[df['Country'] == 'Algeria', 'happiness_score'].min()

arg_mean = df.loc[df['Country'] == 'Argentina', 'happiness_score'].mean()
alg_mean = df.loc[df['Country'] == 'Algeria', 'happiness_score'].mean()

arg_std = df.loc[df['Country'] == 'Argentina', 'happiness_score'].std()
alg_std = df.loc[df['Country'] == 'Algeria', 'happiness_score'].std()

# print(afg_max, afg_min, afg_mean, afg_std)
# print(alg_max, alg_min, alg_mean, alg_std)

output_arg = np.random.normal(loc=arg_mean, scale=arg_std, size=500)
output_alg = np.random.normal(loc=alg_mean, scale=alg_std, size=500)

# count_1, bins_1, ignored_1 = plt.hist(output_arg, 100)
# count_2, bins_2, ignored_2 = plt.hist(output_alg, 100)
# plt.show()

# 7
res = df
res['Year'] = res['Year'].astype('int')
# res = res.loc[df['Year'] == 2015]
# print(res)
# res.plot(y='happiness_score', x='Year')
# res['happiness_score'] = res.groupby('Year')['happiness_score'].transform(lambda x: x.mean())
# res.plot(y='happiness_score', x='Year')
# plt.show()

# task11
"""
CREATE TABLE countries_happiness_score
(
    id SERIAL PRIMARY KEY,
    country VARCHAR(30),
    happiness_score NUMERIC(6, 4),
    year INTEGER
);
"""

"""
INSERT INTO 
countries_happiness_score (country, happiness_score, year)
VALUES 
('Afghanistan',	3.79399991,	2015),
('Afghanistan',	3.575,	2016),
('Afghanistan',	3.203,	2017),
('Afghanistan',	3.632,	2018),
('Afghanistan',	3.354179985,	2019),
('Afghanistan',	2.566900015,	2020),
('Albania',	4.644000053,	2015),
('Albania',	4.959,	2016),
('Albania',	4.719,	2017),
('Albania',	4.771940004,	2018),
('Albania',	4.655,	2019),
('Albania',	4.882699966,	2020)
;
"""

# task12
"""
CREATE OR REPLACE PROCEDURE procedure_1()
language plpgsql
as $$
declare
    x integer := 0;
    year_plus integer := 1940;
begin
    while x <= 3600 loop
        insert into countries_happiness_score(country, happiness_score, year)
        values 
        ('Afghanistan',	random() * 6.300 + 4.605,	2020),
        ('Albania',	random() * 6.300 + 4.605,	2015);
        x :=  x + 1;
        year_plus := year_plus + 1;
        perform pg_sleep(1);
    end loop;
end;
$$;
"""
